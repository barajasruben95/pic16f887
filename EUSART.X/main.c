/*
 * File:   main.c
 * Author: Ruben
 *
 * Created on 10 de abril de 2024, 21:57
 */


#include <xc.h>
#include <stdio.h>

// Internal frequency will be 8MHz
#define _XTAL_FREQ 8000000

// PIC16F887 Configuration Bit Settings
// 'C' source line config statements

// CONFIG1
#pragma config FOSC = INTRC_NOCLKOUT  // Oscillator Selection bits (INTOSCIO oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN)
#pragma config WDTE = OFF             // Watchdog Timer Enable bit (WDT disabled and can be enabled by SWDTEN bit of the WDTCON register)
#pragma config PWRTE = ON             // Power-up Timer Enable bit (PWRT enabled)
#pragma config MCLRE = OFF            // RE3/MCLR pin function select bit (RE3/MCLR pin function is digital input, MCLR internally tied to VDD)
#pragma config CP = OFF               // Code Protection bit (Program memory code protection is disabled)
#pragma config CPD = OFF              // Data Code Protection bit (Data memory code protection is disabled)
#pragma config BOREN = ON             // Brown Out Reset Selection bits (BOR enabled)
#pragma config IESO = ON              // Internal External Switchover bit (Internal/External Switchover mode is enabled)
#pragma config FCMEN = ON             // Fail-Safe Clock Monitor Enabled bit (Fail-Safe Clock Monitor is enabled)
#pragma config LVP = OFF              // Low Voltage Programming Enable bit (RB3 pin has digital I/O, HV on MCLR must be used for programming)

// CONFIG2
#pragma config BOR4V = BOR21V         // Brown-out Reset Selection bit (Brown-out Reset set to 2.1V)
#pragma config WRT = OFF              // Flash Program Memory Self Write Enable bits (Write protection off)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

uint8_t dataTx[50];
uint8_t dataRx;
    
void EUSART_Init(void)
{
    TRISCbits.TRISC6 = 0;   // C6-TX as digital output
    TRISCbits.TRISC7 = 1;   // C7-RX as digital input
    
    /* SPBRG and SPBRGH */
    // Desired baudrate = 9600
    // Fosc = 8Mhz
    // Sync = 0, BRGH = 0, BRG16 = 0
    // SPBRG = 12
    SPBRG = 12;
    
    /* TXSTA register */
    TXSTAbits.SYNC = 0;   // Asyncronous mode
    
    /* RCSTA */
    RCSTAbits.SPEN = 1;   // Configures RX and TX pines as serial
    
    //Transmission enabled
    TXSTAbits.TXEN = 1;
    // Reception enabled
    RCSTAbits.CREN = 1;
}

void EUSART_Write(uint8_t data)
{
    //Send data
    while(TXSTAbits.TRMT == 0);   // TRMT bit is set when TSR is empty
    TXREG = data;
    __nop();                // Wait 1 TCY after writing to TXREG
    while(PIR1bits.TXIF == 0);       // Wait for EUSART TX buffer empty
}

void EUSART_WriteString(uint8_t *data)
{
    while(*data != '\0')
    {
        EUSART_Write(*data++);
    }
}

uint8_t EUSART_Read(void)
{
    uint8_t status;
    
    while(PIR1bits.RCIF == 0);       // Wait until data is available
    status = RCSTA;
    if((status | _RCSTA_OERR_MASK) == 1)
    {
        // Overrun flag set
        // Can be cleared by clearing CREN
        RCSTAbits.CREN = 0;
        // Reception enabled again
        RCSTAbits.CREN = 1;
    }
    return RCREG;
}

void bufclean(void)
{
    uint8_t i;
    for(i = 0; i < sizeof(dataTx); i++)
    {
        dataTx[i] = 0;
    }
}

void main(void) {
    
    uint8_t message[] = "Hello World from PIC16F887\n\r";
    
    // Enable 8MHz
    // OSCCON by default selects 4MHz as the main frequency, change to 8MHz
    OSCCONbits.IRCF = 0b111;

    EUSART_Init();
    EUSART_WriteString(message);
    
    while(1)
    {
        //Send received data
        dataRx = EUSART_Read();
        sprintf((char *)dataTx, (char *)"Received data: %c\n\r", dataRx);
        EUSART_WriteString(dataTx);
        bufclean();
    }
    
    return;
}
