/*
 * File:   main.c
 * Author: Ruben
 *
 * Created on 10 de abril de 2024, 20:05
 */

#include <xc.h>

// Internal frequency will be 8MHz
#define _XTAL_FREQ 8000000

// PIC16F887 Configuration Bit Settings
// 'C' source line config statements

// CONFIG1
#pragma config FOSC = INTRC_NOCLKOUT  // Oscillator Selection bits (INTOSCIO oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN)
#pragma config WDTE = OFF             // Watchdog Timer Enable bit (WDT disabled and can be enabled by SWDTEN bit of the WDTCON register)
#pragma config PWRTE = ON             // Power-up Timer Enable bit (PWRT enabled)
#pragma config MCLRE = OFF            // RE3/MCLR pin function select bit (RE3/MCLR pin function is digital input, MCLR internally tied to VDD)
#pragma config CP = OFF               // Code Protection bit (Program memory code protection is disabled)
#pragma config CPD = OFF              // Data Code Protection bit (Data memory code protection is disabled)
#pragma config BOREN = ON             // Brown Out Reset Selection bits (BOR enabled)
#pragma config IESO = ON              // Internal External Switchover bit (Internal/External Switchover mode is enabled)
#pragma config FCMEN = ON             // Fail-Safe Clock Monitor Enabled bit (Fail-Safe Clock Monitor is enabled)
#pragma config LVP = OFF              // Low Voltage Programming Enable bit (RB3 pin has digital I/O, HV on MCLR must be used for programming)

// CONFIG2
#pragma config BOR4V = BOR21V         // Brown-out Reset Selection bit (Brown-out Reset set to 2.1V)
#pragma config WRT = OFF              // Flash Program Memory Self Write Enable bits (Write protection off)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

volatile uint32_t counter = 0;

void __interrupt(high_priority) tcInt(void)
{
    if (INTCONbits.T0IE && INTCONbits.T0IE) {  // Any timer 0 interrupts?
        INTCONbits.T0IF = 0; // Clear TMR0IF
        TMR0 = 96;           //Reload timer again
        counter++;
    }
}

void main(void) {
    
    // Enable 8MHz
    // OSCCON by default selects 4MHz as the main frequency, change to 8MHz
    OSCCONbits.IRCF = 0b111;
    
    // PORTB0 pull ups disables
    // Int on falling edge
    // T0CS Freq from internal FSync/4
    // Increment on low to high transition
    // Preescaler assigned to TMR0
    // Preescaler 32. (8MHz/4) / 32 = 62500 Hz = 16us 
    OPTION_REG = 0b10000100;
    
    // Timer0 module register to 32. Interrupt in 96*16us = 1.5ms aprox
    TMR0 = 96;
            
    // B0 with digital 0
    PORTBbits.RB0 = 0;
    
    // B0 as digital output
    TRISBbits.TRISB0 = 0;
    
    // B0 as digital, not analog
    ANSELHbits.ANS12 = 0;
    
    // Enable all unmasked interrupts
    // Timer0 overflow interrupt enable
    INTCON = 0b10100000;
    
    while(1)
    {
        // Wait 333 * 1.5ms = 0.5s aprox
        if(counter > 333)
        {
            //Toogle B0
            PORTBbits.RB0 ^= 1;
            counter = 0;
        }
    }
    
    return;
}
